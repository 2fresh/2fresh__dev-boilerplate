import { UserGql } from './typedefs/UserGql';
import { IContext } from '../interfaces/IContext';
import UserService from '../services/UserService';
import { User } from '../models/User';
import { SchemaRoot, Query, Context, compileSchema, Mutation } from 'typegql';
import { AuthMutation } from './typedefs/AuthMutation';
import { Container } from 'typedi';

@SchemaRoot()
class RootSchema {
    private userService: UserService = Container.get(UserService)
    @Query({type: [UserGql] })
    async users(@Context context: IContext): Promise<UserGql[]> {

        // Get all users from DB
        const userModels: User[] = await this.userService.getUsers()

        return this.userService.getMultipleUserGqlFromModels(userModels)
    }

    @Mutation()
    auth(): AuthMutation {
      return new AuthMutation();
    }
}

export default compileSchema(RootSchema)
