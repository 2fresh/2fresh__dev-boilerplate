let error = false;
const mustBeSet: string [] = [
    'DATABASE_URL'
]

mustBeSet.forEach((envVar: string) => {
    if (process.env[envVar] === undefined) {
        error = true;
        console.error(` ☠️  Env ${envVar} must be set!`)
    }
})

if (error) {
    process.exit(1)
}

export default {
    /**
     * Your favorite port
     */
    port: "3000",

    db: {
        url: process.env.DATABASE_URL
    },

    // /**
    //  * Your secret sauce
    //  */
    // jwtSecret: process.env.JWT_SECRET,

    /**
     * Used by winston logger
     */
    logs: {
      level: process.env.LOG_LEVEL || 'silly',
    }
  };

