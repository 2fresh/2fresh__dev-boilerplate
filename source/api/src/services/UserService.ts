import { Service } from 'typedi'
import { User } from './../models/User'
import { UserGql } from '../schema/typedefs/UserGql';

@Service()
export default class UserService {

    getUsers(): Promise<User[]> {
        return User.findAll()
    }

    getUser(id: number): Promise<User> {
        return User.findOne({ where: {id: id} })
    }

    /**
     * @param userModel Database User entity
     * @returns Graphql type definition model
     */
    getUserGqlFromModel(userModel: User): UserGql {

        if (userModel) {
            const gqlModel: UserGql = new UserGql()
            gqlModel.id = userModel.getId()
            gqlModel.username = userModel.getUsername()
            return gqlModel
        }

        throw new Error("User model is undefined!");
    }

    /**
     * @param userModels Array of database User entities
     * @returns Array of graphql type definition models
     */
    getMultipleUserGqlFromModels(userModels: User[]): UserGql[] {
        let result: UserGql[] = []
        if (userModels) {
            userModels.forEach(model => {
                result.push(this.getUserGqlFromModel(model))
            })
            return result
        }

        throw new Error("User models are undefined!");
    }
}
