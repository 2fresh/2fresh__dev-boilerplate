import 'reflect-metadata'
import { GraphQLServer } from 'graphql-yoga';
import schema from './schema'
import config from './config'
import loaders from './loaders'
import LoggerInstance from './loaders/logger';

async function startServer() {
  const server = new GraphQLServer({
    schema,
    context: (req: any) => ({
      ...req
    })
  })


  await loaders({ app: server.express })

  server.start({
    port: config.port,
    playground: "/playground"
  }, (err: any) => {

    // if (err) {
    //   console.error(err)
    // }
    LoggerInstance.info(`✌️  Server is running on localhost:${config.port}`)
  })
}

startServer();
