import Logger from './logger'
import expressLoader from './express';
import { Container } from 'typedi';
import databaseLoader from './database';
import * as express from 'express';

export default async ({ app }: {app: express.Application}) => {

    // Init database and add to DI container
    const db = await databaseLoader();
    Container.set('dbInstance', db)
}
