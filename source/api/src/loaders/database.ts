import { Sequelize } from 'sequelize-typescript'
import config from './../config';
import LoggerInstance from './logger';
import models from './../models'

export default async (): Promise<any> => {
    const url:string = config.db.url || ""
    const db = new Sequelize(url)
    db.addModels(models)
    await db
        .authenticate()
        .then(() => {
            LoggerInstance.info('💾  Connection has been established successfully.');
        })
        .catch((err: any) => {
            LoggerInstance.error('☠️  Unable to connect to the database:', err);
        });

    await db.sync().then(() => {
        LoggerInstance.info('💾  Database synced.');
    })

    return db
}

