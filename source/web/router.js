import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Homepage = () => import('~/pages/homepage').then(m => m.default || m)

// eslint-disable-next-line import/prefer-default-export
export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [{ path: '', name: 'homepage', component: Homepage }]
  })
}
