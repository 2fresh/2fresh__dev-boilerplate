export default interface Book {
  author: string
  title: string
}
