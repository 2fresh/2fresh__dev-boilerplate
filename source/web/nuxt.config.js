const pkg = require('./package')

module.exports = {
  buildDir: '.build/nuxt-dist',
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [
    {
      src: '~/assets/styles/base.scss',
      lang: 'scss'
    }
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '~/modules/routes',
    'nuxt-webfontloader',
    '@nuxtjs/apollo',
    'nuxt-purgecss'
  ],

  /*
  ** purgeCSS https://github.com/FullHuman/purgecss
  */
  purgeCSS: {
    // your settings here
    // whitelistPatterns: [/^blend/, /^fade/, /^mt-/]
  },
  /*
   ** Axios module configuration
   */
  // axios: {
  //   // See https://github.com/nuxt-community/axios-module#options
  // },

  axios: {
    proxy: true, // Can be also an object with default options,
    prefix: '/api/' // for proxy issue
  },

  apollo: {
    includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
    clientConfigs: {
      default: {
        httpEndpoint: `${process.env.API_URL}/`,
        httpLinkOptions: {
          credentials: 'same-origin'
        }
      }
    }
  },

  proxy: {
    '/api/': {
      target: process.env.API_URL || 'http://localhost:3000',
      pathRewrite: {
        '^/api/': '/'
      }
    }
    // '/uploads/': {
    //   target: process.env.API_URL || 'http://localhost:3003'
    // }
  },

  /*
   ** Webfontloader configuration
   ** here you can load webfonts
   ** see: https://github.com/typekit/webfontloader
   */
  webfontloader: {
    google: {
      families: ['Noto Sans:400:latin-ext', 'Noto Serif:400:latin-ext']
    }
  },

  /*
   ** Build configuration
   */
  build: {
    loaders: {
      scss: {
        data: `@import '~/assets/styles/utilities.scss';`
      }
    },
    extractCSS: true,
    postcss: {
      'postcss-preset-env': {},
      plugins: {
        tailwindcss: 'tailwind.config.js'
      }
    },
  }
}
