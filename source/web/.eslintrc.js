module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  extends: ['airbnb-base', 'plugin:vue/recommended', 'prettier', '@nuxtjs'],
  plugins: ['vue','prettier','@typescript-eslint'],
  settings: {
    'import/resolver': 'nuxt'
  },
  rules: {
    'nuxt/no-cjs-in-config': 'off',
    'prettier/prettier': ['error'],
    'import/extensions': [
      'error',
      'always',
      {
        js: 'never',
        vue: 'never'
      }
    ],
    'import/no-extraneous-dependencies': ['never'],
    'vue/max-attributes-per-line': 0,
    'vue/html-self-closing': 0,
    'vue/html-indent': 0,
    'vue/singleline-html-element-content-newline': 0,
    '@typescript-eslint/no-unused-vars': 'error'
  }
}
