# microsite-nuxt

It's a **skeleton** for our NUXT.js projects

**Without any UI and TEST frameworks!** (For now...)

## Contains

- basic NUXT config (express.js, babel, vuex)
- axios.js
- SASS
- ESlint
- Prettier
- Browser-sync

## Using skeleton in your project

Follow this steps:

- First create new project on Bitbucket, then:

```bash
# clone this repository
$ git clone git@bitbucket.org:2fresh/microsite-nuxt.git

# go to the root of skeleton project and remove remote origin
$ git remote remove origin

# add new remote origin of your project
$ git remote add origin git@bitbucket.org:2fresh/PROJECT-NAME.git

# create clean branch with no history
$ git checkout --orphan BRANCH-NAME

# commit changes
$ git commit -m "Initial commit"

# push changes to the new origin
$ git push --set-upstream origin BRANCH-NAME

```

## Build Setup

Every `yarn` command run in `app/` folder

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

### IDE Setup

It's setup description for **Visual Studio Code** (VS Code).

In `app` folder there is `.vscode` folder which includes:

- `settings.json` - default IDE settings for the project
- `extensions.json` - recommended (**must have** in our case...) extensions for the project

Go to Extension tab and find **Workspace recommendations** (just type @recommended), then install all extensions listed.

**!Never rewrite files in `.vscode` and make sure you won't commit changes in this folder!**

#### workspace > user:

Workspace (project) settings will **rewrite** your user settings for VSCode.

You will find your user VSCode settings in:

- Windows - `%APPDATA%\Code\User\settings.json`
- OSx - `$HOME/Library/Application Support/Code/User/settings.json`
- Linux - `$HOME/.config/Code/User/settings.json`

### Debuging

In **VS Code** open debug tab and add configuration of NodeJs environment.

Configuration should look like this:

```{
linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Attach to Nuxt",
      "port": 9229
    }
  ]
}
```

#### usage

1. Run `yarn dev-debug`
2. Put somewhere to the file this line of code `debugger // eslint-disable-line`
3. Make breakpoints you want
4. Press play icon in debug tab

## TODOs

- TEST framework
- enhance debugging (get rid of debugger word)


TODO:
- pridat tailwind
- vytvorit zakladni balicek komponent, ktere pujdou prepouzivat (jako wireframes)
- na development instalovat i dev dependencies
- vytvorit vlastni node alpine image pro web s nuxtem
- doladit precommit linty pro api a web
- yarn install pri dev:start - upravit image aby nebyly zavisle na env
- prepsat config tak aby byl kompatibilni s CI kdyz nejaka promenna chybi, doplni se defaultni
- napojit typescript
- do API GraphQl
