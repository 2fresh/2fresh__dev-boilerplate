#!/usr/bin/env bash

########################################################################################################################
## BASE PROJECT SETUP SECTION
########################################################################################################################

export PROJECT_NAME='test-nuxt'

# ENVIRONMETN MODE [dev|prod]
export ENV='prod'

# CMS IMAGE
export DOCKER__API__IMAGE="2fresh/test-nuxt/api:latest"
export DOCKER__API__VIRTUAL_HOST="api.test-nuxt.development"
export DOCKER__WEB__LETS_ENCRYPT_MAIL="api.test-nuxt.development"

# WEB IMAGE
export DOCKER__WEB__IMAGE="2fresh/test-nuxt/web:latest"
export DOCKER__WEB__VIRTUAL_HOST="test-nuxt.development"
export DOCKER__WEB__LETS_ENCRYPT_MAIL="lets-encrypt@triskihp.development"

#DATABASE
export DOCKER__DB__ROOT_PASSWORD="root"
export DOCKER__DB__DATABASE="test"
export DOCKER__DB__USER="user"
export DOCKER__DB__PASSWORD="password"
