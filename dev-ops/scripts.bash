#!/usr/bin/env bash

# Exit script on first error
set -e

## NORMALIZE PATH
#########################################################################################################################
# Get current dir (from call)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#########################################################################################################################
## NORMALIZE PATH - END

## LOAD CONFIG (DEFAULT + LOCAL)
source ${DIR}/config/default.bash
LOCAL_CONFIG="${DIR}/config/local.bash"
if [ -f ${LOCAL_CONFIG} ]; then
    source ${LOCAL_CONFIG}
fi

########################################
# FUNCTIONS
########################################
function build {

  echo "----------------"
  echo "Building project"
  echo "----------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    --file ${DIR}/docker/docker-compose.build.yml \
    build \
    --no-cache \
    --parallel \
    --force-rm
}

function developmentStart {

  echo "----------------"
  echo "Start development"
  echo "----------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    --file ${DIR}/docker/docker-compose.dev.yml \
    --file ${DIR}/docker/docker-compose.proxy-server.yml \
    up \
    --detach \
    --force-recreate
}

function developmentStop {

  echo "----------------"
  echo "Stop development"
  echo "----------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    --file ${DIR}/docker/docker-compose.dev.yml \
    --file ${DIR}/docker/docker-compose.proxy-server.yml \
    down
}

function go {

  echo "----------------------"
  echo "Go into service: $1"
  echo "----------------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    --file ${DIR}/docker/docker-compose.dev.yml \
    --file ${DIR}/docker/docker-compose.proxy-server.yml \
    exec $1 sh
}

function exec {

  CMD=${@:2}

  echo "----------------------"
  echo "Execute cmd in service: $1, cmd: \"$CMD\" "
  echo "----------------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    --file ${DIR}/docker/docker-compose.dev.yml \
    --file ${DIR}/docker/docker-compose.proxy-server.yml \
    exec -T $1 $CMD
}


function log {
  echo "----------------"
  echo "Run logs $@"
  echo "----------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    logs \
    -f $@
}
function restart {
  echo "----------------"
  echo "Run restart $@"
  echo "----------------"

  docker-compose \
    --project-name=${PROJECT_NAME} \
    --file ${DIR}/docker/docker-compose.yml \
    restart \
    $@
}


CMD=$1

if [[ "$CMD" == "build" ]]; then build; exit; fi
if [[ $CMD == "development-start" ]]; then developmentStart; exit; fi
if [[ $CMD == "development-stop" ]]; then developmentStop; exit; fi
if [[ $CMD == "log" ]]; then log ${@:2}; exit; fi
if [[ $CMD == "restart" ]]; then restart ${@:2}; exit; fi
if [[ $CMD == "go" ]]; then go $2; exit; fi
if [[ $CMD == "exec" ]]; then exec $2 ${@:3}; exit; fi

echo "Running script $CMD";
echo "build"
echo "development-start"
echo "development-stop"
echo "restart | restart <serviceName[api|db|web]>"
echo "log | log <serviceName[api|db|web]>"
echo "go <serviceName[api|db|web]>"
echo "exec <serviceName[api|db|web]> \"cmd\"

exit;
